<?php

/**
 * This exposed form plugin allows to create exposed forms where only one exposed
 * filter can be applied per time.
 *
 * Users selects the filter to apply (i.e, Book title, or author, or category...)
 * and then write the info in the textfield that appears next to the select box.
 *
 * When click on filter, the view is filtered only by the selected filter.
 */
class views_plugin_exposed_form_only_one_filter extends views_plugin_exposed_form_basic {

  /**
   * This text will be displayed in the admin UI. 
   *
   * (other values are Basic, and Input required)
   */
  function summary_title() {
    return t('Only one filter');
  }

  /**
   * This function is like an implementation of hook_form_alter() from a module.
   *
   * All exposed forms plugins can implement this function to alter the exposed
   * form of the view.
   */
  function exposed_form_alter(&$form, &$form_state) {
    // Add some custom css
    //drupal_add_css(drupal_get_path('module', 'only_one_filter') . '/only_one_filter.css');

    // Due this class extends other plugin, we have to call to the parent function too
    parent::exposed_form_alter($form, $form_state);

    // For each defined exposed filters of the view
    // Add a dependency to the select box generated at the end
    foreach ($this->view->filter as $id => $handler) {
      if ($handler->can_expose() && $handler->is_exposed()) {
        $info = $handler->exposed_info();
        $filters[$info['value']] = check_plain($info['label']);
        unset($form['#info']['filter-' . $id]);
        $class = $form[$id]['#attributes']['class'];

        $form[$id]['#process'] = array('views_process_dependency');
        $form[$id]['#dependency'] = array(
          'edit-selected-filter' => array($info['value']),
        );
      }
    }

    // Create a select box that will define which filter 
    // will be applied of all available
    $form['selected_filter'] = array(
      '#options' => $filters,
      '#type' => 'select',
      '#prefix' => '<div class="views-exposed-widget">',
      '#suffix' => '</div>',
      '#title' => check_plain($this->options['select_filter_label']),
      '#weight' => -10,
      '#input' => TRUE,
    );

    $class = $form['#attributes']['class'];
    $form['#attributes']['class'] = $class . ' only-one-filter';
  }

  /**
   * Tell Views what options this plugin can store.
   *
   * In Views 3, all data stored with a plugin MUST be defined as part of
   * the option_definition() or Views will neither save nor export that
   * data. This allows us to define the defaults for our options. When
   * Views sees that the user has set data to the default, if that default
   * is later changed, all Views using that value will also change. This
   * makes it simpler to propagate simple updates.
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['selected_filter'] = array('default' => 'Select a filter', 'translatable' => TRUE);
    return $options;
  }

  /**
   * This form element will be added to the default options of
   * views_plugin_exposed_form_basic
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['select_filter_label'] = array(
      '#type' => 'textfield',
      '#title' => t('Label for filter selector'),
      '#default_value' => $this->options['selected_filter'],
    );
  }

  /**
   * This function is executed when user clic on the 'Apply' button of view
   * the exposed form.
   *
   * In this case, when users click on Apply, the exposed form only pass the
   * information of the filter that is selected in the select box.
   * 
   * If a view has four exposed filters, views will send four values, one per
   * filter.
   *
   * This function overides the value of this data if the filter is not the
   * selected in the select box. So views thinks they are empty and doesn't
   * apply the exposed filter to the view.
   */
  function exposed_form_submit(&$form, &$form_state, &$exclude) {
    $selected_filter = $this->view->exposed_input['selected_filter'];
    if (is_array($this->view->filter)) {
      foreach ($this->view->filter as $id => $filter) {
        if ($filter->is_exposed() && $id != $selected_filter) {
          $this->view->exposed_data[$id] = array();
        }
      }
    }

    // This values are excluded from the url.
    $exclude[] = 'selected_filter';
    $exclude[] = 'filter_value';
  }

}
