<?php

/**
 * Implementation of hook_views_plugins().
 */
function only_one_filter_views_plugins() {
  return array(
    'exposed_form' => array( // Here is defined whay kind of plugin we are providing
      'only_one_filter' => array(
        'title' => t('Only one filter'),
        'help' => t('Users can select only one exposed filter per time'),
        'handler' => 'views_plugin_exposed_form_only_one_filter',
        'uses options' => TRUE,
        'help topic' => 'exposed-form-only-one-filter',
        'parent' => 'basic',
      ),
    ),
  );
}
