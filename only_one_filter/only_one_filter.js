Drupal.behaviors.only_one_filter = function(context) {

  $("select#edit-selected-filter").change(function () {
    $("input#edit-filter-value").val('').focus();
    
  });
  
  $("input#edit-filter-value").keyup(function(event){
    $("form.only-one-filter input.form-autocomplete").val($(this).val());
  });

}
