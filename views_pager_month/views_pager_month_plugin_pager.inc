<?php

/**
 * Example plugin to handle paging by month.
 */
class views_pager_month_plugin_pager extends views_plugin_pager {
  /**
   * This kind of pager does not need to count the number of records.
   */
  function use_count_query() {
    return FALSE;
  }

  /**
   * Because we don't know how many pages there are, we never believe there
   * are more records.
   */
  function has_more_records() {
    return FALSE;
  }

  /*
   * Tell Views what this pager's title is.
   *
   * The summary title is what you click on when selecting the pager in the
   * the main Views UI. For normal pagers it says something like,
   * Paged, 10 items. In our case, it always says the same thing.
   */
  function summary_title() {
    return t('By month');
  }

  /**
   * Tell Views what options this plugin can store.
   *
   * In Views 3, all data stored with a plugin MUST be defined as part of
   * the option_definition() or Views will neither save nor export that
   * data. This allows us to define the defaults for our options. When
   * Views sees that the user has set data to the default, if that default
   * is later changed, all Views using that value will also change. This
   * makes it simpler to propagate simple updates.
   */
  function option_definition() {
    $options = parent::option_definition();

    // Provide options for what $_GET keys to fetch the month and year from.
    $options['month'] = array('default' => 'month');
    $options['year'] = array('default' => 'year');

    // Left in as an exercise: Expose this pager.
    // The normal pager can be exposed. This one theoretically could too.
    // However, to expose it we would probably need to create a range of
    // dates or something.
//    $options['expose'] = array(
//      'contains' => array(
//        'items_per_page' => array('default' => FALSE, 'bool' => TRUE),
//        'items_per_page_label' => array('default' => 'Items per page', 'translatable' => TRUE),
//        'items_per_page_options' => array('default' => '5, 10, 20, 40, 60'),
//        'items_per_page_options_all' => array('default' => FALSE),
//        'items_per_page_options_all_label' => array('default' => '- All -', 'translatable' => TRUE),
//
//        'offset' => array('default' => FALSE, 'bool' => TRUE),
//        'offset_label' => array('default' => 'Offset', 'translatable' => TRUE),
//      ),
//    );
    return $options;
  }

  /*
   * Provide the form for setting options.
   */
  function options_form(&$form, &$form_state) {
    $form['month'] = array(
      '#title' => t('Month attribute'),
      '#type' => 'textfield',
      '#description' => t('The query attribute to fetch month data from in the URL.'),
      '#default_value' => $this->options['month'],
      '#required' => TRUE,
    );

    $form['year'] = array(
      '#title' => t('Year attribute'),
      '#type' => 'textfield',
      '#description' => t('The query attribute to fetch year data from in the URL.'),
      '#default_value' => $this->options['year'],
      '#required' => TRUE,
    );
  }

  /**
   * This is the heart and soul of the pager.
   *
   * This determines what "page" we're on (year and month) and then
   * modifies the query. Because this is a very naive version, the
   * code is annotated where it is known that it should be improved to
   * be a good working pager.
   */
  function query() {
    // First, make sure that there is actually a node available. If not
    // we will bail immediately.
    $this->table_alias = $this->view->query->ensure_table('node');
    if (empty($this->table_alias)) {
      return;
    }

    // By fetching our data from the exposed input, it is possible to
    // feed pager data through some method other than $_GET.
    $input = $this->view->get_exposed_input();

    // We store the month and year on $this to make it easy for the
    // renderer to see what ended up selected.

    // Currently we are using only year in the form of YYYY.
    if (empty($input[$this->options['year']])) {
      $this->year = format_date(time(), 'custom', 'Y', 0);
    }
    else {
      $this->year = $input[$this->options['year']];
    }

    // This pager supports the month as a digit from 1 to 12 only.
    if (empty($input[$this->options['month']])) {
      $this->month = format_date(time(), 'custom', 'n', 0);
    }
    else {
      $this->month = $input[$this->options['month']];
    }

    // Now, turn the month and year into two dates for the filter.
    $start = mktime(0, 0, 0, $this->month, 1, $this->year);
    // Figure out next month:
    $next_month = $this->month + 1;
    $next_year = $this->year;

    // Roll it over if we passed 12.
    if ($next_month == 13) {
      $next_month = 1;
      $next_year++;
    }

    // The last day of the month is the same as the 0th day of the month
    // after it.
    $end = mktime(23, 59, 59, $next_month, 0, $next_year);

    // @todo We are not adjusting this to the user timezone. That is left as
    // an exercise.

    // Finally, add the filter.
    $this->view->query->add_where(0, "$this->table_alias.created >= %d AND $this->table_alias.created <= %d", $start, $end);
  }

  function render($input) {
    // This adds all of our template suggestions based upon the view name and display id.
    $pager_theme = views_theme_functions('views_pager_month', $this->view, $this->display);
    // Then, using that, just pass through to the theme.
    return theme($pager_theme, $this, $input);
  }
}
