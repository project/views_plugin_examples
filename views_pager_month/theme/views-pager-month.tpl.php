<?php
/**
 * @file
 * Template file for the example display.
 *
 * Variables available:
 * - $plugin: The pager plugin object. This contains the view as well as a lot
 *   of other potential data.
 * - $next_month: A link to the next month.
 * - $next_year: A link to the same month in the next year.
 * - $prev_month: A link to the previous month.
 * - $prev_year: A link to the previous year.
 * - $current_page: A visual of the date for the current page.
 */
?>
<div class="views-pager-month">
  <div class="prev-year"><?php print $prev_year; ?></div>
  <div class="prev-month"><?php print $prev_month; ?></div>
  <div class="current-page"><?php print $current_page; ?></div>
  <div class="next-month"><?php print $next_month; ?></div>
  <div class="next-year"><?php print $next_year; ?></div>
</div>