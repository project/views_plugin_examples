<?php

/**
 * @file
 * Views include file containing views hooks.
 *
 * This file is included automatically by Views, on-demand. It is registered
 * using hook_views_api() in the primary module, and the code is only loaded
 * when views hooks are needed.
 */

/**
 * Implementation of hook_views_plugins().
 */
function views_pager_month_views_plugins() {
  // This pager is really only valid for nodes, but there is no way to
  // limit pagers based upon base tables. Maybe this should be a feature
  // request against Views.
  return array(
    'pager' => array(
      'views_pager_month' => array(
        'title' => t('EXAMPLE: Post date month'),
        'help' => t('Page by the month the node was posted.'),
        'handler' => 'views_pager_month_plugin_pager',
        'help topic' => 'pager-month',
        'uses options' => TRUE,
      ),
    ),
  );
}
