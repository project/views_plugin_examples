<?php

/**
* @file
* Views area text handler.
*/

/**
 * @ingroup views_area_handlers Views' area handlers
*/

class views_area_sum_handler_sum extends views_handler_area {
  function render($empty = FALSE) {
    // Ensure we have an array.
    $this->totals = array();

    // Calculate the total for all numeric fields.
    foreach ($this->view->style_plugin->rendered_fields as $row_index => $row) {
      foreach ($row as $field_id => $value) {
        if (is_numeric($value)) {
          if (!isset($this->totals[$field_id])) {
            $this->totals[$field_id] = 0;
          }

          $this->totals[$field_id] += $value;
        }
      }
    }

    $output = '';

    // Now, if we ended up with results, render them.
    foreach ($this->totals as $field_id => $total) {
      // See template_preprocess_views_view_fields() for the right way to do this.
      // Actually printing a field properly is relatively involved because of
      // all the semantic options. For the purposes of this example we won't do
      // that, but will just brute force it.
      $field = $this->view->field[$field_id];
      $value = check_plain($field->label());
      if ($value) {
        if ($field->options['element_label_colon']) {
          $value .= ': ';
        }
      }
      $value .= $total;

      // Simple div wrapper that should really be configurable and probably
      // taken from the semantic information on the field.
      $output .= '<div>' . $value . '</div>';
    }

    // This should also be configurable.
    if ($output) {
      $output = '<div><strong>' . t('Totals') . '</strong></div>' . $output;
    }
    return $output;
  }
}
