This is a simple example of a views area handler that adds up all of the
numeric fields and prints out a result.

It is not particularly useful by itself, but it does show how area handlers
are constructed, and how they can access all of the field data if they would
like.