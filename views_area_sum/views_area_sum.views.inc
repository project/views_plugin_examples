<?php
/**
 * @file
 * Provide views data for our example.
 */

/**
 * Implementation of hook_views_data()
 */
function views_area_sum_views_data() {
  $data['views']['views_area_sum'] = array(
    'title' => t('Sum'),
    'help' => t('Show a row of summed values.'),
    'area' => array(
      'handler' => 'views_area_sum_handler_sum',
    ),
  );
  return $data;
}

/**
 * Implementation of hook_views_handlers()
 */
function views_area_sum_views_handlers() {
  return array(
    'handlers' => array(
      'views_area_sum_handler_sum' => array(
        'parent' => 'views_handler_area',
      ),
    ),
  );
}
